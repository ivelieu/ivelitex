
![cellworkbench](assets/appliedenergistics2/textures/guis/cellworkbench.png)
![chest](assets/appliedenergistics2/textures/guis/chest.png)
![vajra](aesprite/itemVajra.gif)
![terminal](aesprite/larger.gif)
![nettool](aesprite/larger1.gif)

# Ivelitex By Ivelieu
(https://gitlab.com/ivelieu/ivelitex)


Replaces most ae2 GUIs, and some other GUIs I frequently use with fractal art background versions. It is intended to be run with GTNH 2.4.0, but I imagine many of the ae2 GUIs will work on 1.12 versions of ae2. (I may test this later for stability.) In the future I may make a concerted effort for a wide range of version support for ae2, purely based on however much motivation/energy/support I have at the time. 

All the GUIs are made using fractal art that I have personally made, mostly using JWildfire which I self taught to use, and you can find more production information along with creative commons licensing information at https://www.deviantart.com/ivelieu. I make an effort to write details on how I make every individual image there. A few images I used for this texture pack I have not yet uploaded there, it is mostly because my process of uploading is rather disorganised and I have a hard time figuring out what needs to be uploaded - I will upload any files I use, and/or provide JWildfire flame files on request, and all the deviantart images are available for high-res download there.

To make the GUIs from the fractal backgrounds I used paint.NET. For most GUI s I make a second layer, set the original GUI to transparency 92/255, put the fractal behind it, then magic wand to remove excess. For the variable length terminals like wct, they have a repeating pattern, and for each of those I manually raster edited so it looked decent enough. There are still some rough edges for those variable length terminals though, it is just very hard for me to manually edit so that it aligns, but I think it looks good enough now for me to upload. 

It also changes a few items I use frequently, like vajra, gravi chest plate, network tool, network visualisation tool, and wireless crafting terminal to be consistent with ae2 and ae2fc's design. I add a few modest animations using aesprite. I change it to pink because, well, I like pink. Of course, if you don't like any of the changes but you want to keep some, feel free to remove them at your leisure too. Just unzip it and go nuts. 

I have talked to GTNH devs and I have confirmed it is currently and likely permanently impossible to replace GUIs for gregtech machines due to modularUI's implementation. I made a few which you can find in the `\assets\gregtech\textures\gui` folder. 

For anything outside of the variable length ae terminals which I will try to fix when I have the chance, if you find any other bugs, please let me know and post with screenshots. I will try my best to fix them with what is possible in a resource pack :) 

I believe that  high quality free education, software and media is essential for the meaningful proliferation of ideas, success and joy in the world, which is why I choose to use permissive licensing for my work. You can read more about the philosophy of free software and media here, which I highly encourage: https://www.gnu.org/philosophy/philosophy.html. I also find this story in particular very visceral, relatable, and empowering: https://www.gnu.org/philosophy/right-to-read.html. 

All of the work in this texture pack produced by me is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. I encourage anyone else who chooses to publicly publish media or code to choose a license recommended by the Free Software Foundation and the GNU community. 

You can find a high-res collage of my work here: https://www.deviantart.com/ivelieu/art/Labelled-Portfolio-Montage-978203202

Some of the GUI s I started with the Dark UI by Jimbno made for GTNH, which can be found here: https://github.com/Jimbno/Dark.UI
I also used some of the transparent UI by Sampsa, which in turn also used Dark UI by Jimbno: https://github.com/S4mpsa/TransparentUI

Besides not being able to render textured GT multiblock guis, these are bugs I am aware of, but don't have a fix for currently:
- I don't know how to change the font color of text rendered ingame on the GUIs (like saying 'this is an ME chest'), so it's kinda low contrast but still readable.
- there is a small line jutting out of interface terminal at the bottom but I don't know how to fix.

